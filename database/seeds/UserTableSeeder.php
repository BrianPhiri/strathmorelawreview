<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
           'name' => 'Cecil Abungu',
           'email' => 'cecil.abungu@strathmore.edu',
           'password' => bcrypt('secret'), 
        ]);
    }
}
