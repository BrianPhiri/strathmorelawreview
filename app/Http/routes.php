<?php

//auth routes
// Authentication routes...
$this->get('admin/login', 'Auth\AuthController@showLoginForm');
$this->post('admin/login', 'Auth\AuthController@login');
$this->get('auth/logout', 'Auth\AuthController@logout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//resources
Route::resource('article', 'ArticlesController');
Route::resource('member', 'MembersController');

//reader routes
Route::get('/', 'ArticlesController@index');
Route::post('/feedback', 'ArticlesController@feedback');
Route::get('read', 'ArticlesController@read');
Route::post('read', 'ArticlesController@read');
Route::get('archives', 'ArticlesController@archive');
Route::get('members/{mem}', 'MembersController@display');
Route::get('contact', 'InformationController@contact');
Route::get('contactus', function(){ return view('review.contactus'); });
Route::get('aboutus', 'InformationController@about');
Route::get('submits', 'InformationController@viewGuide');
Route::get('style', 'InformationController@viewRules');

//admin routes
Route::group(['middleware' => 'auth'], function () {
    Route::get('published', 'ArticlesController@publishedList');
    Route::get('admin', function(){ return view('editor.index'); });
    Route::get('adminChart', 'SiteReportController@index');
    Route::get('publish', function(){ return view('editor.article.publish'); });

    //guide
    Route::get('submit/guide', 'InformationController@guide');
    Route::get('submit/guide/{id}/edit', 'InformationController@showGuide');
    Route::patch('submit/guide/{id}', 'InformationController@updateGuide');
    Route::get('submit/guide/add', function(){ return view('editor.submit.guide'); });
    Route::post('add/guide', 'InformationController@newGuide');
    //rules
    Route::get('submit/rules', 'InformationController@rule');
    Route::get('submit/rules/{id}/edit', 'InformationController@showRules');
    Route::patch('submit/rules/{id}', 'InformationController@updateRules');
    Route::get('submit/rules/add', function(){ return view('editor.submit.rules'); });
    Route::post('add/rules', 'InformationController@newRules');

    //about
    Route::get('information/about', function(){ return view('editor.aboutus.about'); });
    //feeback
    Route::get('/feedback', 'InformationController@viewFeedback');
});

// TODO: work on archive articles

Route::get('/home', 'HomeController@index');

Route::get('/test', 'InformationController@viewFeedback');