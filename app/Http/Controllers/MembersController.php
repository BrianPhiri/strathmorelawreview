<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Members;
use Storage;
use Redirect;

class MembersController extends Controller
{
    public function display($mem){
      $members = Members::where('role', '=', $mem)->get();
      return view('review.members', compact('members'));
    }
    public function index(){
      $members = Members::All();
      return view('editor.member.members', compact('members'));
    }

    public function create(){
      return view('editor.member.add');
    }

    public function store(Request $input){
      if($input->hasFile('image')){
        $image = $input->file('image');
        $string = $input['fullnames'];
        $imageName = preg_replace('/\s+/', '', $string);
        $upload = $imageName.'-'.str_random(10);
        $destinationPath = 'images/memebers/'.$upload;
        $uploaded = Storage::disk('images')->put($destinationPath, file_get_contents($image->getRealPath()));
        if($uploaded){
          Members::create([
            'fullnames' => $input['fullnames'],
            'about' => $input['about'],
            'image' => $upload,
            'role' => $input['role']
          ]);
          return redirect('member');
        }else{
          return "sorry could not save information";
        }
      }else{
          return "sorry could not save information";
      }
    }

    public function show($id){
        $member = Members::findOrFail($id);
        return view('editor.member.edit', compact('member'));
    }

    public function update($id, Request $post){
        $image = '';
        $member = Members::findOrFail($id);
        if($post['image'] == ''){
            $image = $member->image;
        }else{
            $image = $post['image'];
        }

        if($post['image'] == ''){
            $image = $member['image'];
        }else{
            $image = $post->file('image');
            $string = $post['fullnames'];
            $imageName = preg_replace('/\s+/', '', $string);
            $upload = $imageName.'-'.str_random(10);
            $destinationPath = 'images/memebers/'.$upload;
            $uploaded = Storage::disk('images')->put($destinationPath, file_get_contents($image->getRealPath()));
            if($uploaded){
                $image = $destinationPath;
            }else{
                return "sorry could not save information";
            }
        }

        $member->fullnames = $post['fullnames'];
        $member->about = $post['about'];
        $member->image = $image;
        $member->role = $post['role'];
        $member->save();
        return Redirect::intended('member');
    }

    public function destroy($id){
        Members::destroy($id);
        return Redirect::intended('/member');
    }
}
