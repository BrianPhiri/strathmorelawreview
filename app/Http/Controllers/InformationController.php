<?php

namespace App\Http\Controllers;

use Request;
use Redirect;

use LaravelAnalytics;
use App\Http\Requests;
use App\Information;
use App\submission_guide;
use App\submission_rules;
use App\About;
use App\Feedback;
use App\Articles;

class InformationController extends Controller
{
  public function contact(){
    return view('review.contacts');
  }

  public function about(){
    return view('review.aboutus');
  }

  public function guide(){
      $guide = submission_guide::latest()->take(1)->first();
      return view('editor.submit.viewguide', compact('guide'));
  }

  public function rule(){
      $rules = submission_rules::latest()->take(1)->first();
      return view('editor.submit.viewrules', compact('rules'));
  }

  // post guid
  public function newGuide(){
    $input = Request::all();
    submission_guide::create($input);
    return Redirect::intended('admin');
  }
  // post rules
  public function newRules(){
    $input = Request::all();
    submission_rules::create($input);
    return Redirect::intended('admin');
  }


//  editing the guide and rules

    //guide
    public function showGuide($id){
        $guide = submission_guide::findOrFail($id);
        return view('editor.submit.editguide', compact('guide'));
    }

    public function updateGuide($id){
        $input = Request::all();
        $guide = submission_guide::findOrFail($id);
        $guide->instructions = $input['instructions'];
        $guide->save();
        return Redirect::intended('submit/guide');
    }

    //rules
    public function showRules($id){
        $rules = submission_rules::findOrFail($id);
        return view('editor.submit.editrules', compact('rules'));
    }
    public function updateRules($id){
        $input = Request::all();
        $rules = submission_rules::findOrFail($id);
        $rules->rules = $input['rules'];
        $rules->save();
        return Redirect::intended('submit/rules');
    }

//end edit
  public function viewGuide(){
    $guide = submission_guide::latest()->take(1)->get();
    return view('review.guide', compact('guide'));
  }

  public function viewRules(){
    $rules = submission_rules::latest()->take(1)->get();
    return view('review.rules', compact('rules'));
  }

  //view the feedbacks from readers
    public function viewFeedback(){
        //$feedback = Feedback::all();
        $feedbacks = Articles::with('feedback')->latest()->simplePaginate(5);
        return view('editor.feedback.feedback', compact('feedbacks'));
    }
}
