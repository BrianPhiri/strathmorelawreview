<?php

namespace App\Http\Controllers;

// use Request;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Articles;
use Storage;
use Redirect;
use Response;
use App\Feedback;

class ArticlesController extends Controller
{
    public function index(){
      $articles = Articles::latest()->simplePaginate(5);
      return view('review.home', compact('articles'));
    }

    public function show($id){
      $article = Articles::findOrFail($id);
      return view('review.read', compact('article'));
    }

    public function store(Request $post){
      if($post->hasFile('file')){
        $paper = $post->file('file');
        $string = $post['title'];
        $paperName = preg_replace('/\s+/', '', $string);
        $paperName = preg_replace('/[^A-Za-z0-9 _]/','', $paperName);
        $paperName = $paperName.'-'.date('dmy');
        $destinationPath = 'uploads/articles/'.$paperName.'.pdf';
        $uploaded = Storage::put($destinationPath, file_get_contents($paper->getRealPath()));
        if($uploaded){
          Articles::create([
            'author' => $post['author'],
            'title' => $post['title'],
            'abstruct' => $post['abstruct'],
            'main_body' => $paperName
          ]);
        }else{
          return "sorry could not upload your pdf";
        }
        return redirect('published');
      }
    }

    public function edit($id){
      $article = Articles::findOrFail($id);
      return view('editor.article.edit', compact('article'));
    }

    public function update($id, Request $post){
        $pdfArticle = '';
        $article = Articles::findOrFail($id);

        if($post['main_body'] == '') {
            $pdfArticle = $article->main_body;
        }else{
            $pdfArticle = $post['main_body'];
        }
        if($post['main_body'] == ''){
            $destinationPath = $article->main_body;
        }else{
            $paper = $pdfArticle;
            $string = $post['title'];
            $paperName = preg_replace('/\s+/', '', $string);
            $paperName = preg_replace('/[^A-Za-z0-9 _]/','', $paperName);
            $paperName = $paperName.'-'.date('dmy');
            $destinationPath = 'uploads/articles/'.$paperName.'.pdf';
            $uploaded = Storage::put($destinationPath, file_get_contents($paper->getRealPath()));
            if($uploaded){
                $pdfArticle = $destinationPath;
            }else{
                return "sorry could not upload your pdf";
            }
        }

        $article->author = $post['author'];
        $article->title = $post['title'];
        $article->abstruct = $post['abstruct'];
        $article->main_body = $destinationPath;
        $article->save();

        return Redirect::intended('published');

    }

    public function publishedList(){
      $articles = Articles::latest()->simplePaginate(20);
      return view('editor.article.articleList', compact('articles'));
    }

    public function read(Request $input){
      $paper = $input['article'];
      $path = storage_path().'/app/uploads/articles/'.$paper.'.pdf';
      return Response::make(file_get_contents($path), 200, [
          'Content-Type' => 'application/pdf',
          'Content-Disposition' => 'inline; filename="'.$paper.'"'
      ]);
    }

    public function feedback(Request $input){
      Feedback::create([
        'article_id' => $input['article'],
        'mainBody'   => $input['comment']
      ]);
      return redirect('/');
    }
    
    public function archive(){
      $articles = Articles::select('id', 'author', 'title', 'main_body')->get();
      return view('review.archives', compact('articles'));
    }

    public function destroy($id){
        Articles::destroy($id);
        return Redirect::intended('/admin');
    }
}
