<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class submission_guide extends Model
{
    protected $table = 'submission_guide';
    protected $fillable = [ 'instructions' ];
}
