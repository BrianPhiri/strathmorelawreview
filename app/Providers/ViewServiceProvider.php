<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Articles;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {

      view()->composer('review._sidebar', function ($view) {
            $view->with('articles', Articles::latest()->take(5)->get());
      });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
