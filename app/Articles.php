<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Articles extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'title', 'author', 'main_body', 'abstruct'
    ];

    protected $dates = ['deleted_at'];

    public function feedback(){
        return $this->hasMany('App\Feedback', 'article_id','id');
    }
}
