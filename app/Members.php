<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Members extends Model
{
    use SoftDeletes;

    protected $table = 'editorial';
    protected $fillable = [
      'fullnames', 'about', 'image', 'role'
    ];
    protected $dates = ['deleted_at'];
}
