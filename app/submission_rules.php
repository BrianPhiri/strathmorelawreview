<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class submission_rules extends Model
{
    protected $table = 'submission_rules';
    protected $fillable = [ 'rules' ];
}
