<!DOCTYPE html>
<html>
  <head>
    <title>Law review</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- styles -->
    <link href="{{ URL::asset('css/adminstyles.css') }}" rel="stylesheet">
    <!-- wysihtml5 -->
    <link href="https://raw.githubusercontent.com/jhollingworth/bootstrap-wysihtml5/master/src/bootstrap-wysihtml5.css" media="all">
    <!-- datatables -->
    <link  href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="all">

    <script  src="{{ asset('js/jquery.min.js') }}"></script>
    <script  src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
    <script src="js/custom.js"></script>
    <script src="js/editors.js"></script>
    <script src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="https://raw.githubusercontent.com/jhollingworth/bootstrap-wysihtml5/master/src/bootstrap-wysihtml5.js"></script>

  </head>
  <body>
  	<div class="header">
	     <div class="container">
	        <div class="row">
	           <div class="col-md-5">
	              <!-- Logo -->
	              <div class="logo">
	                 <h1><a href="{{ URL::asset('admin') }}">Strathmore Law School</a></h1>
	              </div>
	           </div>
	           <div class="col-md-5">
	              <div class="row">
	                <div class="col-lg-12">
	                </div>
	              </div>
	           </div>
	           <div class="col-md-2">
	              <div class="navbar navbar-inverse" role="banner">
	                  <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	                    <ul class="nav navbar-nav">
	                      <li class="dropdown">
	                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}<b class="caret"></b></a>
	                        <ul class="dropdown-menu animated fadeInUp">
	                          {{--<li><a href="profile.html">Profile</a></li>--}}
	                          <li><a href="{{URL('auth/logout')}}">Logout</a></li>
	                        </ul>
	                      </li>
	                    </ul>
	                  </nav>
	              </div>
	           </div>
	        </div>
	     </div>
	</div>
  <div class="page-content">
    <div class="row">
    <div class="col-md-2">
      <div class="sidebar content-box" style="display: block;">
              <ul class="nav">
                  <!-- Main menu -->
                  <li class="current"><a href="{{ URL::asset('admin') }}"><i class="glyphicon glyphicon-home"></i>Home</a></li>
                  <li class=""><a href="{{ URL::asset('feedback') }}"><i class="glyphicon glyphicon-inbox"></i>FeedBack</a></li>
                  <li class="submenu">
                      <a href="#">
                          <i class="glyphicon glyphicon-list"></i> Publications
                          <span class="caret pull-right"></span>
                      </a>
                      <!-- Sub menu -->
                      <ul>
                          <li><a href="{{ URL::asset('published') }}"><i class="glyphicon glyphicon-calendar"></i>Published articles</a></li>
                          <li><a href="{{ URL::asset('publish') }}"><i class="glyphicon glyphicon-pencil"></i>Publish new articles</a></li>
                      </ul>
                  </li>
                  <li class="submenu">
                      <a href="#">
                          <i class="glyphicon glyphicon-list"></i> Members
                          <span class="caret pull-right"></span>
                      </a>
                      <!-- Sub menu -->
                      <ul>
                          <li><a href="{{ URL::asset('member') }}"><i class="glyphicon glyphicon-calendar"></i>View members</a></li>
                          <li><a href="{{ URL::asset('member/create') }}"><i class="glyphicon glyphicon-pencil"></i>Add new member</a></li>
                      </ul>
                  </li>

                  <li class="submenu">
                       <a href="#">
                          <i class="glyphicon glyphicon-list"></i> Guide
                          <span class="caret pull-right"></span>
                       </a>
                       <!-- Sub menu -->
                       <ul>
                          <li><a href="{{ URL::asset('submit/guide') }}">Style Guide</a></li>
                          <li><a href="{{ URL::asset('submit/rules')}}">Call for submission</a></li>
                      </ul>
                  </li>
              </ul>
           </div>
    </div>
  @yield('content')
</div>
  <footer>
       <div class="container">

          <div class="copy text-center">
             Copyright {{ date('y-m-d') }} <a href='#'> | By Phiri, Brian</a>
          </div>

       </div>
    </footer>

  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('js/custom.js') }}"></script>
  <script type="text/javascript">
  $(document).ready(function(){
  	$('#myTable').DataTable({
  		"bPaginate": false
  	});
  });
  </script>
</body>
</html>
