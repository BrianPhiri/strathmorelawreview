<!DOCTYPE HTML>
<html>
<head>
<title>SLS</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{ URL::asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link href="{{ URL::asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
<link  href="http://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" media="all">
<link href='http://fonts.googleapis.com/css?family=Monda' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Candal' rel='stylesheet' type='text/css'>
<script  src="{{ asset('js/jquery.min.js') }}"></script>
<script  src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
<script src="http://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>

<script  src="{{ asset('js/bootstrap.min.js') }}"></script>
</head>
<body>
<div class="header">
	<div class="header_top">
		<div class="wrap">
			<!--<div class="logo">
			     <a href=""><img src="{{ URL::asset('images/slslogo.png') }}" alt="" /></a>
			</div>-->
			<center><h1 style="color:white;">STRATHMORE LAW REVIEW</h1></center>
			<div class="clear"></div>
		</div>
	</div>
	<div class="header_bottom">
		<div class="wrap">
			<div class="menu">
			    <ul>
			    	<div class="btn-group">
			    		<li><a href="{{ URL::asset('/') }}">Home</a></li>
			    	</div>
						<!--  -->
						<div class="btn-group">
						  <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Editorial Board
						  </button>
						  <ul class="dropdown-menu">
						    <li><a href="{{ URL::asset('members/Board') }}">Board</a></li>
						    <li><a href="{{ URL::asset('members/Assistants') }}">Editorial Assistants</a></li>
						    <li><a href="{{ URL::asset('members/Alumni') }}">Alumni</a></li>
						  </ul>
						</div>
						<!--  -->
						<!-- button for submissions -->
						<div class="btn-group">
							<button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Submissions
							</button>
							<ul class="dropdown-menu">
								<li><a href="{{ URL::asset('style') }}">Style Guide</a></li>
								<li><a href="{{ URL::asset('submits') }}">Call for Submissions</a></li>
							</ul>
						</div>
						<!--  -->
			    	<div class="btn-group">
			    		<li><a href="{{ URL::asset('contact') }}">Contact Us</a></li>
			    	</div>
						<div class="btn-group">
							<li><a href="{{ URL::asset('archives') }}">Archives</a></li>
						</div>
			    </ul>
			</div>
			<!-- <div class="search_box">
			    <form>
			    <input type="text" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}"><input type="submit" value="">
			    </form>
			</div> -->
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="wrap">
	<div class="main">
		<div class="content">
      @yield('content')
    </div>
			@include('review._sidebar')
	<div class="clear"></div>
	</div>
</div>
<div class="footer">
	<div class="wrap">
		<div class="footer_grid1">
			<h3>About Us</h3>
			<p>Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here desktop publishing making it look like readable English.<br><a href="{{ URL::asset('aboutus')}}">Read more....</a></p>
		</div>
		<div class="footer_grid2">
			<h3>Navigate</h3>
				<div class="f_menu">
					<ul>
				       <li><a href="/">Home</a></li>
				       <li><a href="contactus">Contact</a></li>
				   </ul>
				</div>
		</div>
	<div class="footer_grid3">
		<h3>Get us on our Social pages</h3>
		<div class="img_list">
		    <ul>
		     <li><img src="images/facebook.png" alt="" /><a href="#">Facebook</a></li>
		     <li><img src="images/google.png" alt="" /><a href="#">Google</a></li>
		    </ul>
		</div>
	</div>
	</div>
<div class="clear"></div>
</div>
	<div class="f_bottom">
		<p>© 2016 rights Reseverd | Built by<a href=""> Phiri, Brian</a></p>
	</div>
</body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-78290338-1', 'auto');
  ga('send', 'pageview');

</script>
</html>
