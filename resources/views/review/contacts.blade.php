@extends('layouts.readerlayout')
@section('content')
<div style="padding: 40px;">
  <div class="clear"></div>
  <div class="clear"></div>
  <address>
    <strong>Strathmore Law School</strong><br>
    Madaraka Estate<br>
    Ole Sangale Road, PO Box 59857,<br>
    00200 City Square,<br>
    Nairobi, Kenya<br>
    <abbr title="Phone">Phone Numbers:</abbr> 0703-034000, 0703-034200
  </address>

  <address>
    <strong>Email</strong><br>
    <a href="mailto:#">email@example.com</a>
  </address>
</div>
@endsection
