@extends('layouts.readerlayout')
@section('content')
			<div class="box1">
			  <h2><a href="/article/{{$article->id}}">{!! $article->title !!}</a></h2> <br>
			  <span>By {{ $article->author }}- {{ $article->created_at}}</span> <br>
				<h3><u>Abstruct</u></h3>
			  <div class="data_desc">{!! $article->abstruct !!}</div>
				<hr>
				<h3><u>Main Body</u></h3>
				<div class="data_desc">{!! $article->main_body !!}</div>
			</div>
@endsection
