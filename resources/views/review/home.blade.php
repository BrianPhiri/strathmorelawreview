@extends('layouts.readerlayout')
@section('content')
<style>
	input[type="search"]{
	  	-webkit-border-radius: 5px;
  		-moz-border-radius: 5px;
  		border-radius: 5px;
	}
</style>
<br><br>
		<table id="myTable">
			<thead>
				<tr>
					<td>
					</td>
				</tr>
			</thead>
			@foreach($articles as $article)
					<tr>
						<td>
							<div class="box1">
							    <h2>{{ $article->title }}</h2> <br>
							    <div class="row">
							        <div class="group1 col-sm-6 col-md-6">
												 <span>By {{ $article->author }}</span>
												- <span class="glyphicon glyphicon-time"></span> uploaded on - {{ date('F d, Y', strtotime($article->created_at)) }}
							        </div>
							        <div class="group2 col-sm-6 col-md-6">
												<span class="glyphicon glyphicon-envelope"></span>
												<button type="button" class="btn-link" data-toggle="modal" data-target="#myModal{{ $article->id }}">
												  <span>Add a comment>></span>
												</button>
												<!-- Modal -->
												{!! @Form::open(array('action' => 'ArticlesController@feedback')) !!}
													<div class="modal fade" id="myModal{{ $article->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel{{ $article->id }}">
														<div class="modal-dialog" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
																	<h4 class="modal-title" id="myModalLabel{{ $article->id }}">{{ $article->title }}</h4>
																</div>
																<div class="modal-body">
																		<div class="feedback">
																				<input type="hidden" name="article" value="{!! $article->id !!}">
																			   <label>Add Comment </label>
																				 {{ @Form::textarea('comment') }}
																			   <br>
																		</div>
																		<div class="clear"></div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																	<input type="submit" class="btn btn-primary" name="submit" value="Add Comment">
																</div>
															</div>
														</div>
													</div>
												{!! @Form::close() !!}
												<!-- end model -->

							        </div>
							    </div>
							    <hr>
							    <br />
									<div class="data">
									    <p>{{ $article->abstruct }}</p>
											{!! @Form::open(array('action' => 'ArticlesController@read')) !!}
	                      <input type="hidden" name="article" value="{!! $article->main_body !!}">
												{!! @Form::submit('Continue reading >>>', [ 'class' => 'btn btn-link']) !!}
									    <!-- <a href="{{ URL::asset('article') }}/{{$article->id}}">Continue reading >>></a> -->
											{!! @Form::close() !!}
									</div>
							</div>
						</td>
					</tr>
			@endforeach
		</table>
		{{ $articles->links() }}

<script type="text/javascript">
$(document).ready(function(){
	$('#myTable').DataTable({
		"bPaginate": false
	});
});
</script>
@endsection
