@extends('layouts.readerlayout')
@section('content')
<div class="clear"></div>
<div class="data_desc">
  <article>
    <p>
    @foreach($guide as $gd)
      {!! $gd->instructions !!}
    @endforeach
  </p>
  </article>
</div>
@endsection
