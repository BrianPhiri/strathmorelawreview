@extends('layouts.readerlayout')
@section('content')
<div class="clear"></div>
<div class="data_desc">
   <table class="strips" id="myTable">
       <thead>
           <tr>
              <td><b>Author</b></td>
              <td><b>Title</b></td>
              <td><b>Action</b></td>     
           </tr>
       </thead>
       <tbody>
           @foreach($articles as $article)
            <tr>
                <td>{{ $article->author }}</td>
                <td>{{ substr($article->title,0,60) }}...</td>

                <td>
                    {!! @Form::open(array('action' => 'ArticlesController@read')) !!}
                    <input type="hidden" name="article" value="{{ $article->main_body }}">
                    {!! @Form::submit('Download', [ 'class' => 'btn btn-warning']) !!}
                    {!! @Form::close() !!}
                </td>
            </tr>
            <br>
           @endforeach
       </tbody>
   </table>
</div>
@endsection