@extends('layouts.readerlayout')
@section('content')
<div style="height: 830px;" class="section-background col-xs-12" data-mh="match-testimonial">
  <h2>Members</h2>

  <!-- Testimonial box 1 -->
  @foreach($members as $member)
  <div class="testimonial-box clearfix">
    <div>
      <img class="img-circle" src="{{URL::asset('images/memebers')}}/{{ $member->image }}" alt="">
    </div>
    <div>
      <p>{{ $member->about }}</p>
      <h4>{{ $member->fullnames }}</h4>
    </div>
  </div>
  @endforeach
</div>
@endsection
