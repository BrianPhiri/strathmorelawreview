@extends('layouts.readerlayout')
@section('content')
<div class="clear"></div>
<div class="data_desc">
  <article>
    <p>
    @foreach($rules as $rule)
      {!! $rule->rules !!}
    @endforeach
  </p>
  </article>
</div>
@endsection
