<div class="sidebar">
  <div class="side_top">
      <h2>Recent Articles</h2>
    <div class="list1">
       <ul>
         {!! @Form::open(array('action' => 'ArticlesController@read')) !!}
           @foreach($articles as $article)
             <input type="hidden" name="article" value="{!! $article->main_body !!}">
             <input type="submit" style="color: gray;" class="btn btn-link" name="read" value="{{ substr($article->title,0,60) }}...">
           @endforeach
         {!! @Form::close() !!}
      </ul>
    </div>
  </div>
</div>
