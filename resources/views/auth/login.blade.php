<!DOCTYPE HTML> 
 <html> 
     <head> 
         <link href="{{ URL::asset('css/auth.css') }}" rel="stylesheet" type="text/css" media="all"/> 
         <script  src="{{ asset('js/auth.js') }}"></script> 
     </head> 
     <body> 
         <div class="login-page"> 
         <div class="form"> 
             <form method="POST" action="/admin/login"> 
                 {!! csrf_field() !!} 
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-6">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="col-md-4 control-label">Password</label>
                <div class="col-md-6">
                    <input type="password" class="form-control" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

             <button type="submit">Login</button> 
             <!--<p class="message">Not registered? <a href="#">Create an account</a></p>--> 
             </form> 
         </div> 
         </div> 
     </body> 
 </html> 



