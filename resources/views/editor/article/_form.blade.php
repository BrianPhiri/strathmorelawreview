
<div class="content-box-large">
  <div class="col-md-6 form-group">
    {!! Form::label('title', 'Tile') !!}
    {!! Form::text('title', null, ['class' => 'form-control','placeholder' => 'Title of the Article','required' => '']) !!}
  </div>
  <br><br><br><br><br>
  <div class="col-md-6 form-group">
    {!! Form::label('author', 'Author') !!}
    {!! Form::text('author', null, ['class' => 'form-control','placeholder' => 'Name of the Author','required' => '']) !!}
  </div>
  <br><br><br><br><br>
  <div class="col-md-6 form-group">
    {!! Form::label('abstruct', 'Abstruct') !!}
    {!! Form::textarea('abstruct', null, ['class' => 'form-control','placeholder' => 'Type the abstract of the document...']) !!}
  </div>
  <div class="panel-body">
    {!! Form::label('Main_Body', 'Main Body') !!}
    {!! Form::file('file') !!}
  </div>
</div>
