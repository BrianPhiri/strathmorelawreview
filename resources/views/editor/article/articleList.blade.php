@extends('layouts.adminlayout')
@section('content')
<div class="col-md-10">

  <div class="content-box-large">
        <div class="">
          <table class="table table-hover" id="myTable">
            <thead>
              <tr>
                <td>Title</td> <td>Author</td> <td>Created at</td> <td>Actions</td><td></td><td></td><td></td>
              </tr>
            </thead>
          <tbody>
            @foreach($articles as $article)
              <tr>
                <td>{{ substr($article->title,0,60) }}...</td> <td>{{ $article->author }}</td> <td>{{ date('F d, Y', strtotime($article->created_at)) }}</td>
                <td><input type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal{{ $article->id }}" value="View"> </td>
                 <td><a href="{{ URL::asset('article')}}/{{ $article->id }}/edit " class="btn btn-sm btn-primary" type="button"> Edit</a></td>
                 <td>
                    {!! Form::open( array('method' => 'DELETE', 'url' => 'article/'.$article->id) ) !!}
                        <hidden vlaue="{{$article->id}}"></hidden>
                        <input type="submit" id="" class="btn btn-danger" value="Delete">
                    {!! Form::close() !!}  </td>
                </td>
								<!-- Modal -->
								<div class="modal fade" id="myModal{{ $article->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="myModalLabel{{ $article->id }}">Article</h4>
											</div>
											<div class="modal-body">
												<label> Title </label> <br>
                        <p>{{ $article->title }}</p>
                        <label> Author </label> <br>
                        <p>{{ $article->author }}</p>
                        <label>Date of Submission </label> <br>
                        <p>{{ $article->created_at }}</p>
                        <label> Abstruct: </label> <br>
                        <p>{{ $article->abstruct }}</p>

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Done Viewing</button>
											</div>
										</div>
									</div>
								</div>
								<!-- end modal -->
              </tr>
            @endforeach
          </tbody>
          </table>
          {{ $articles->links() }}
        </div>
  <br><br>
  </div>
</div>
</div>
@endsection
