@extends('layouts.adminlayout')
@section('content')

<div class="col-md-10">
  {!! Form::open(array('url' => 'article', 'files' => true )) !!}
    @include('editor.article._form')
  {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
  {!! Form::close() !!}
</div>

<script src='http://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea'
});
</script>
@endsection
