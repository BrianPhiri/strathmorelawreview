@extends('layouts.adminlayout')
@section('content')

<div class="col-md-10">
  {!! Form::model($article , array('method' => 'PATCH', 'url' => 'article/'. $article->id )) !!}
    @include('editor.article._form')
  {!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}
  {!! Form::close() !!}
</div>

<script src='http://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea'
});
</script>
@endsection
