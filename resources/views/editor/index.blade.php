@extends('layouts.adminlayout')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.js" charset="utf-8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.1.4/Chart.bundle.js" charset="utf-8"></script>
<div class="col-md-10">
	<div class="content-box-large">
		<div class="row">
			{{--<canvas id="canvas" width="100" height="100"></canvas>--}}

		</div>
	</div>

	<div class="content-box-large">
		<div class="row">
			<canvas id="canvas" width="100" height="30"></canvas>
			<script>
				$.get("{{ URL::asset('/adminChart')}}", function(data){
					var views = [], visits = [];
					$.each(data, function(index, repObj){
						views.push(repObj.pageViews);
						visits.push(repObj.visitors);
					});

					var data = {
						labels: visits,
						datasets: [
							{
								label: "Site views",
								fill: false,
								lineTension: 0.1,
								backgroundColor: "rgba(75,192,192,0.4)",
								borderColor: "rgba(75,192,192,1)",
								borderCapStyle: 'butt',
								borderDash: [],
								borderDashOffset: 0.0,
								borderJoinStyle: 'miter',
								pointBorderColor: "rgba(75,192,192,1)",
								pointBackgroundColor: "#fff",
								pointBorderWidth: 1,
								pointHoverRadius: 5,
								pointHoverBackgroundColor: "rgba(75,192,192,1)",
								pointHoverBorderColor: "rgba(220,220,220,1)",
								pointHoverBorderWidth: 2,
								pointRadius: 1,
								pointHitRadius: 10,
								data: views,
							}
						]
					};
					var ctx = document.getElementById("canvas");
					var ctx = document.getElementById("canvas").getContext("2d");
					var ctx = $("#canvas");

					var myLineChart = new Chart(ctx, {
						type: 'line',
						data: data,
						options: {
							xAxes: [{
								display: true
							}]
						}
					});

				});
			</script>
		</div>
	</div>
</div>
</div>
@endsection
