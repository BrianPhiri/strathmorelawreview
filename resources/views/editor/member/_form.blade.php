<div class="form-group">
  {!! Form::label('image', 'Image', array('class' => 'col-sm-2 control-label')) !!}
  <div class="col-sm-10">
    {!! Form::file('image') !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('role', 'Role', array('class' => 'col-sm-2 control-label')) !!}
  <div class="col-sm-10">
    {!! Form::select('role', array('Board' => 'Board', 'Assistants' => 'Editorial Assistants', 'Alumni' => 'Alumni')); !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('fullname', 'Full Names', array('class' => 'col-sm-2 control-label')) !!}
  <div class="col-sm-10">
    {!! Form::text('fullnames', null, ['class' => 'form-control', 'placeholder' => 'Full Member Names']) !!}
  </div>
</div>
<div class="form-group">
  {!! Form::label('description', 'Description', array('class' => 'col-sm-2 control-label')) !!}
  <div class="col-sm-10">
    {!! Form::textarea('about', null, ['class' => 'form-control', 'rows' => '3' , 'placeholder' => 'About the memeber...']) !!}
  </div>
</div>
<br>

<br><br>
<div class="form-group">
  <div class="col-sm-10">
    {!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}
  </div>
</div>
