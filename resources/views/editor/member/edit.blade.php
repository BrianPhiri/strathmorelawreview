@extends('layouts.adminlayout')
@section('content')

  {!! Form::model($member, array('method' => 'PATCH', 'url' => 'member/'.$member->id )) !!}
    @include('editor.member._form', ['submitButtonText' => 'Edit memeber'])
  {!! Form::close() !!}
@endsection
