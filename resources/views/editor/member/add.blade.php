@extends('layouts.adminlayout')
@section('content')

  {!! Form::open(array('url' => '/member', 'files' => true )) !!}
    @include('editor.member._form', ['submitButtonText' => 'Add memeber'])
  {!! Form::close() !!}
@endsection
