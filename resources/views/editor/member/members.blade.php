@extends('layouts.adminlayout')
@section('content')
<div class="col-md-10">

  <div class="content-box-large">
        <div class="">
          <table class="table table-hover">
            <thead>
              <tr>
                <td>Names</td><td>Role</td><td>Created at</td><td>Actions</td> <td></td><td></td><td></td>
              </tr>
            </thead>
          <tbody>
            @foreach($members as $member)
              <tr>
                <td>{{ $member->fullnames}}</td>
                <td>{{ $member->role}}</td>
                <td>{{ date('F d, Y', strtotime($member->created_at)) }}</td>
                <td><input type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal{{ $member->id }}" value="View"></td>
                <td><a href="{{ URL::asset('member')}}/{{$member->id}}" class="btn btn-primary">Edit</a></td>
                <td>
                    {!! Form::open( array('method' => 'DELETE', 'url' => 'member/'.$member->id) ) !!}
                        <hidden vlaue="{{$member->id}}"></hidden>
                        <input type="submit" id="" class="btn btn-danger" value="Delete">
                    {!! Form::close() !!}  </td>
                </td>
								<!-- Modal -->
								<div class="modal fade" id="myModal{{ $member->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="myModalLabel{{ $member->id }}">Memebers</h4>
											</div>
											<div class="modal-body">
												<label> Name </label>
                        <p>{{ $member->fullnames }}</p>
                        <label> Memebership </label>
                        <p>{{ $member->role }}</p>  
                        <label> Information </label>
                        <p>{{ $member->about }}</p>

											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-default" data-dismiss="modal">Done Viewing</button>
											</div>
										</div>
									</div>
								</div>
								<!-- end modal -->
              </tr>
            @endforeach
          </tbody>
          </table>
        </div>
  <br><br>
  </div>
</div>
</div>
@endsection
