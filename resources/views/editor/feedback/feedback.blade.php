@extends('layouts.adminlayout')
@section('content')
    <div class="col-md-10">
        <div class="content-box-large">
            <table id="myTable">
                <thead><h3>FeedBack</h3></thead>
                <tbody>
                    @foreach($feedbacks as $feedback)
                        <blockquote>
                            <dl>
                                <dt>{{ $feedback->title }}</dt>
                                <dd>
                                    @foreach($feedback->feedback as $fb)
                                    <blockquote>
                                        <p class="text-warning">{{ $fb->mainBody}}</p>
                                    </blockquote>
                                    @endforeach
                                </dd>
                            </dl>
                        </blockquote>
                    @endforeach
                </tbody>
            </table>
            {{ $feedbacks->links() }}
        </div>
    </div>
@endsection