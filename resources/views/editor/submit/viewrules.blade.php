@extends('layouts.adminlayout')
@section('content')
    <div class="col-md-10">
        <div class="content-box-large">
            <div class="row">
                <a href="{{ URL::asset('submit/rules') }}/{{$rules->id}}/edit" class="btn btn-primary">Edit</a>
                <a href="{{ URL::asset('submit/rules/add') }}" class="btn btn-warning">New</a>
            </div>
            <hr>
            <div class="row">
                {!! $rules->rules !!}
            </div>
        </div>
    </div>
@endsection