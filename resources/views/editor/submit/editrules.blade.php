@extends('layouts.adminlayout')
@section('content')

<div class="col-md-10">
  {!! Form::model($rules,array('method' => 'PATCH', 'url' => 'submit/rules/'.$rules->id)) !!}
    @include('editor.submit._rulesform')
  {!! Form::close() !!}
</div>

<script src='http://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea'
});
</script>
@endsection
