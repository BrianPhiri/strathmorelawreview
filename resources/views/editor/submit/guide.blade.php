@extends('layouts.adminlayout')
@section('content')

<div class="col-md-10">
  {!! Form::open(array('url' => 'add/guide', 'files' => true )) !!}
    @include('editor.submit._guideform')
  {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
  {!! Form::close() !!}
</div>

<script src='http://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea'
});
</script>
@endsection


