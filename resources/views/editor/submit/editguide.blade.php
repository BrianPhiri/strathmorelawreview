@extends('layouts.adminlayout')
@section('content')

<div class="col-md-10">
  {!! Form::model($guide, array('method' => 'PATCH', 'url' => 'submit/guide/'.$guide->id)) !!}
    @include('editor.submit._guideform')
  {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
  {!! Form::close() !!}
</div>

<script src='http://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea'
});
</script>
@endsection


