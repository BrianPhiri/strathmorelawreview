
<div class="content-box-large">
  <div class="col-md-6 form-group">
    {!! Form::label('title', 'Rules') !!}
    {!! Form::textarea('rules', null, ['id' => 'mytextarea', 'class' => 'form-control' ,'placeholder' => 'Type the Instruction...']) !!}
 </div>
  <div class="panel-body">
    
  </div>
    <div class="col-md-6 form-group">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
  </div>
</div>
<script>
  $('#mytextarea').wysihtml5();
</script>
