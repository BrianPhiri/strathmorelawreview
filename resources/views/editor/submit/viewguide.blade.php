@extends('layouts.adminlayout')
@section('content')
    <div class="col-md-10">
        <div class="content-box-large">
            <div class="row">
                <a href="{{URL::asset('submit/guide')}}/{{ $guide->id }}/edit" class="btn btn-primary">Edit</a>
                <a href="{{ URL::asset('submit/guide/add') }}" class="btn btn-warning">New</a>
            </div>
            <hr>
            <div class="row">
                {!! $guide->instructions !!}
            </div>
        </div>
    </div>
@endsection