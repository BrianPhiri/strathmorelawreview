<div class="content-box-large">
  <br>
  <div class="col-md-6 form-group">
    {!! Form::label('about', 'Add information') !!}
    {!! Form::textarea('about', null, ['class' => 'form-control','placeholder' => 'What are you about?...']) !!}
  </div>
  <br><br>
  <div class="col-md-6 form-group">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
  </div>
</div>
