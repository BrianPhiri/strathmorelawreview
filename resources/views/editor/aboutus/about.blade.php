@extends('layouts.adminlayout')
@section('content')

<div class="col-md-10">
  {!! Form::open(array('url' => 'information/about', 'files' => true )) !!}
    @include('editor.aboutus._form')
  {!! Form::close() !!}
</div>

<script src='http://cdn.tinymce.com/4/tinymce.min.js'></script>
<script>
tinymce.init({
  selector: '#mytextarea'
});
</script>
@endsection
